
import com.raul.buffer.client.Consumidor;

/**
 * Aplicação responsável por rodar consumidores
 */
public class ConsumidorApplication {

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Parâmetros inválidos");
            return;
        }
        int threads;
        int porta;
        try {
            threads = Integer.parseInt(args[0]);
            porta = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("Parâmetros inválidos");
            return;
        }
        String ip = args[1];

        for (int i = 0; i < threads; i++) {
            new Thread(new Consumidor(ip, porta, i + 1)).start();
        }
    }
}
