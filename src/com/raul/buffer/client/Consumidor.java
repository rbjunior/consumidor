package com.raul.buffer.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cliente consumidor do buffer
 */
public class Consumidor extends ClienteBuffer {

    /**
     * Constr�i um novo consumidor
     *
     * @param ip IP do servidor de buffer
     * @param porta porta do servidor de buffer
     * @param id Identificador do consumidor
     */
    public Consumidor(String ip, int porta, int id) {
        super(ip, porta, id);
    }

    /**
     * M�todo respons�vel por retirar um n�mero do buffer via socket
     */
    private void consumir() {
        try {
            ObjectOutputStream saida = new ObjectOutputStream(this.conexao.getOutputStream());
            saida.flush();

            ObjectInputStream entrada = new ObjectInputStream(this.conexao.getInputStream());

            HashMap<String, Object> mensagem = new HashMap();
            mensagem.put("operacao", "-");
            mensagem.put("quem", this.nome);

            saida.writeObject(mensagem);
            long tempoInicial = System.currentTimeMillis();
            saida.flush();
            HashMap<String, Object> mensagemServidor = (HashMap) entrada.readObject();
            long tempoTotal = System.currentTimeMillis() - tempoInicial;

            if (true == ((boolean) mensagemServidor.get("status"))) {
                Integer numero = (Integer) mensagemServidor.get("item");
                System.out.println("Retirado o valor " + numero + " do Buffer pelo " + nome + " em " + tempoTotal + " milissegundos");
            } else {
                if ("BUFFER_VAZIO".equals(mensagemServidor.get("codigo"))) {
                    System.out.println(nome + " tentou retirar item do Buffer vazio");
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Consumidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        if (null != conexao) {
            consumir();
            desconectar();
        }
    }
}
